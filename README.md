# Tweaks

Various mods, some are now obsolete, includes
- enable cyanogenmod call recording
- disable proximity sensor
- hide supersuser icon from status bar
- add app icon to toasts
- hide the user switcher in the extended status bar