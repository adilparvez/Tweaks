package uk.co.adilparvez.tweaks;


import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class XposedModule implements IXposedHookZygoteInit, IXposedHookInitPackageResources, IXposedHookLoadPackage {

    private static XSharedPreferences preferences;

    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        preferences = new XSharedPreferences(Strings.PACKAGE_NAME);

        if (preferences.getBoolean(Strings.PREF_TOAST_KEY, false)) {
            ToastMod.initZygote(startupParam);
        }
    }

    @Override
    public void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam initPackageResourcesParam) throws Throwable {
        if (preferences.getBoolean(Strings.PREF_USERS_KEY, false)) {
            UserSwitcherMod.handleInitPackageResources(initPackageResourcesParam);
        }
        if (preferences.getBoolean(Strings.PREF_RECORDING_KEY, false)) {
            CallRecordingMod.handleInitPackageResources(initPackageResourcesParam);
        }
    }

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        if (preferences.getBoolean(Strings.PREF_PROXIMITY_KEY, false)) {
            ProximitySensorMod.handleLoadPackage(loadPackageParam);
        }
        if (preferences.getBoolean(Strings.PREF_SUPERUSER_KEY, false)) {
            SuIconMod.handleLoadPackage(loadPackageParam);
        }
        if (preferences.getBoolean(Strings.PREF_RECORDING_KEY, false)) {
            CallRecordingMod.handleLoadPackage(loadPackageParam);
        }
    }

}
