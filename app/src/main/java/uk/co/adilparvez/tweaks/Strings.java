package uk.co.adilparvez.tweaks;


public class Strings {

    public static final String PACKAGE_NAME = Strings.class.getPackage().getName();

    public static final String PREF_PROXIMITY_KEY = "uk.co.adilparvez.tweaks-pref_proximity";
    public static final String PREF_TOAST_KEY = "uk.co.adilparvez.tweaks-pref_toast";
    public static final String PREF_USERS_KEY = "uk.co.adilparvez.tweaks-pref_users";
    public static final String PREF_SUPERUSER_KEY = "uk.co.adilparvez.tweaks-pref_superuser";
    public static final String PREF_RECORDING_KEY = "uk.co.adilparvez.tweaks-pref_recording";

}
