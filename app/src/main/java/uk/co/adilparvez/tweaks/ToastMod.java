package uk.co.adilparvez.tweaks;


import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.XResources;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.callbacks.XC_LayoutInflated;

public class ToastMod {

    public static void initZygote(IXposedHookZygoteInit.StartupParam startupParam) throws Throwable {
        XResources.hookSystemWideLayout("android:layout/transient_notification", new XC_LayoutInflated() {
            @Override
            public void handleLayoutInflated(LayoutInflatedParam layoutInflatedParam) throws Throwable {
                LinearLayout linearLayout = (LinearLayout) layoutInflatedParam.view;
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                Context context = linearLayout.getContext();

                TextView textView = (TextView) layoutInflatedParam.view.findViewById(android.R.id.message);
                LinearLayout.LayoutParams layoutParams0 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams0.gravity = Gravity.CENTER;
                textView.setLayoutParams(layoutParams0);

                PackageManager packageManager = context.getPackageManager();

                ImageView imageView = new ImageView(context);
                imageView.setMaxHeight(textView.getHeight() + 128);
                imageView.setMaxWidth(textView.getWidth() + 128);
                imageView.setAdjustViewBounds(true);
                imageView.setImageDrawable(packageManager.getApplicationIcon(context.getPackageName()));
                LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams1.gravity = Gravity.CENTER;
                layoutParams1.rightMargin = 10;
                imageView.setLayoutParams(layoutParams1);

                linearLayout.addView(imageView, 0);
            }
        });
    }

}
