package uk.co.adilparvez.tweaks;


import android.widget.FrameLayout;

import de.robv.android.xposed.callbacks.XC_InitPackageResources;
import de.robv.android.xposed.callbacks.XC_LayoutInflated;

public class UserSwitcherMod {

    public static void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam initPackageResourcesParam) throws Throwable {
        if (!initPackageResourcesParam.packageName.equals("com.android.systemui")) {
            return;
        }

        initPackageResourcesParam.res.hookLayout("com.android.systemui:layout/status_bar_expanded_header", new XC_LayoutInflated() {
            @Override
            public void handleLayoutInflated(LayoutInflatedParam layoutInflatedParam) throws Throwable {
                FrameLayout userSwitcher = (FrameLayout) layoutInflatedParam.view.findViewById(
                        layoutInflatedParam.res.getIdentifier("multi_user_switch", "id", "com.android.systemui"));
                userSwitcher.setVisibility(FrameLayout.INVISIBLE);
            }
        });

        initPackageResourcesParam.res.hookLayout("com.android.systemui:layout/keyguard_status_bar", new XC_LayoutInflated() {
            @Override
            public void handleLayoutInflated(LayoutInflatedParam layoutInflatedParam) throws Throwable {
                FrameLayout userSwitcher = (FrameLayout) layoutInflatedParam.view.findViewById(
                        layoutInflatedParam.res.getIdentifier("multi_user_switch", "id", "com.android.systemui"));
                userSwitcher.setVisibility(FrameLayout.GONE);
            }
        });
    }

}
