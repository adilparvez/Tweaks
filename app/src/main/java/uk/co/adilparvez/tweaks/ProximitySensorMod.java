package uk.co.adilparvez.tweaks;


import android.hardware.Sensor;
import android.util.SparseArray;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class ProximitySensorMod {

    public static void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        if (!loadPackageParam.packageName.equals("android")) {
            return;
        }

        final Class systemSensorManager = XposedHelpers.findClass("android.hardware.SystemSensorManager", loadPackageParam.classLoader);

        XposedHelpers.findAndHookMethod("android.hardware.SystemSensorManager$SensorEventQueue", loadPackageParam.classLoader, "dispatchSensorEvent", int.class, float[].class
                , int.class, long.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                SparseArray<Sensor> sensors = (SparseArray<Sensor>) XposedHelpers.getStaticObjectField(systemSensorManager, "sHandleToSensor");
                int handle = (int) param.args[0];
                Sensor sensor = sensors.get(handle);
                if (sensor.getType() == Sensor.TYPE_PROXIMITY) {
                    float[] values = (float[]) param.args[1];
                    values[0] = 100f;
                    param.args[1] = values;
                }
            }
        });
    }

}
