package uk.co.adilparvez.tweaks;


import android.content.Context;

import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class CallRecordingMod {

    public static void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        if (!loadPackageParam.packageName.equals("com.android.dialer")) {
            return;
        }

        XposedHelpers.findAndHookMethod("com.android.services.callrecorder.CallRecorderService", loadPackageParam.classLoader, "isEnabled", Context.class, XC_MethodReplacement.returnConstant(true));
    }

    public static void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam initPackageResourcesParam) throws Throwable {
        if (!initPackageResourcesParam.packageName.equals("com.android.dialer")) {
            return;
        }

        initPackageResourcesParam.res.setReplacement("com.android.dialer:bool/call_recording_enabled", true);
    }

}